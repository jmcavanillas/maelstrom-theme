const themeDir = __dirname;

const purgecss = require('@fullhuman/postcss-purgecss')({
  // see https://gohugo.io/hugo-pipes/postprocess/#css-purging-with-postcss
  // and https://github.com/dirkolbrich/hugo-theme-tailwindcss-starter
  content: [
    './hugo_stats.json',
    themeDir + '/hugo_stats.json'
  ],
  defaultExtractor: (content) => {
    let els = JSON.parse(content).htmlElements;
    return els.tags.concat(els.classes, els.ids);
  }
})

module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-nested": {},
    tailwindcss: { config: "./themes/maelstrom/tailwind.config.js" },
    autoprefixer: {},
  },
};

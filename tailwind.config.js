const colors = require('tailwindcss/colors');

const Color = require('color')
const alpha = (clr, val) => Color(clr).alpha(val).rgb().string()
const lighen = (clr, val) => Color(clr).lighten(val).rgb().string()
const darken = (clr, val) => Color(clr).darken(val).rgb().string()

module.exports = {
  purge: {
    enabled: process.env.HUGO_ENV === "production",
    content: [
      "./**/**/*.html"
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      info: {
        DEFAULT: '#168ACD',
        light: '#419FD9',
        dark: darken('#168ACD', 0.27)
      },
      cola: {
        light: lighen('#5f4555', 0.27),
        DEFAULT: '#5f4555',
        dark: darken('#5f4555', 0.27),
        darker: darken('#5f4555', 0.35)
      },
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      turquoise: {
        lighter: lighen('#036B70', 0.35),
        light: lighen('#036B70', 0.27),
        DEFAULT: '#036B70',
        dark: darken('#036B70', 0.27),
      },
      red: colors.rose,
    },
    fontFamily: {
      sans: [
        "Roboto",
        "ui-sans-serif",
        "system-ui",
        "Helvetica",
        "Arial",
        "sans-serif",
        "Apple Color Emoji",
        "Segoe UI Emoji",
      ],
      display: [
        "Montserrat",
        "ui-sans-serif",
        "system-ui",
        "Helvetica",
        "Arial",
        "sans-serif",
        "Apple Color Emoji",
        "Segoe UI Emoji",
      ]
    },
    boxShadow: {
      sm: "0 1px 2px 0 rgba(0, 0, 0, 0.05)",
      smbi: "0 1px 2px 0 rgba(0, 0, 0, 0.05), 0 -1px 2px 0 rgba(0, 0, 0, 0.05)",
      DEFAULT:
        "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
      mdbi:
        "0 0px 6px 2px rgba(0, 0, 0, 0.1), 0 2px 4px 2px rgba(0, 0, 0, 0.06)",
      md:
        "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
      lg:
        "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
      xl:
        "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
      "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
      "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
      inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
      none: "none",
      neum:
        "12px 12px 16px 0 rgba(0, 0, 0, 0.25), -8px -8px 12px 0 rgba(255, 255, 255, 0.6);",
    },
    extend: {
      zIndex: {
        "-1": "-1",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/aspect-ratio')
  ],
};
